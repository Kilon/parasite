#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <regex>

#ifdef WIN32
#include <windows.h>
#endif // WIN32

//using namespace std;
char ownPth[MAX_PATH];
HMODULE hModule = GetModuleHandle(NULL);

int main(int argc, char* argv[])
{
	std::streampos size;
	std::string input_line;
	std::string output_line;
	std::ifstream infile;
	std::ofstream outfile;
	std::regex detect_whitespace_regex_pattern("\t.+");
	std::smatch match;
	bool whitespace_detected = false;
	if(argc > 2)
	{
		
		infile.open(argv[1]);
		outfile.open(argv[2]);
		GetModuleFileName(hModule, ownPth, (sizeof(ownPth)));
		int count;
		std::cout << "path is : " << ownPth << std::endl;
		std::cout << "\nCommand-line arguments:\n";
		for (count = 0; count < argc; count++)
			std::cout << "  argv[" << count << "]   " << argv[count] << "\n";
	}
	else
	{
		std::cout << "not enough arguments !!! \ncorrect syntax is: circe [input_file] [export_file]" << std::endl;
		return 0;
	}
	if(infile.is_open())
	{
		
		while ( getline(infile,input_line) )
		{
			std::cout << "**************************" << std::endl;
			std::cout << "size of line : " << input_line.size() << std::endl; 
			std::cout << "input : " <<input_line << '\n';
			if(outfile.is_open())
			{
				if(std::regex_match(input_line,match,detect_whitespace_regex_pattern) && whitespace_detected==false){
					outfile << "{" << std::endl << input_line << std::endl;
					std::cout<<"Output : " << "{" << std::endl << input_line << std::endl;
					whitespace_detected = true;
					std::cout << "first case executed" << std::endl;
				}
				else if (std::regex_match(input_line, match, detect_whitespace_regex_pattern)==false && whitespace_detected == true ) {
					outfile << "}" << std::endl << input_line << std::endl;
					std::cout << "Output : " << "}" << std::endl << input_line << std::endl;
					whitespace_detected = false;
					std::cout << "second case executed" << std::endl;
				}
				else if (std::regex_match(input_line,match, detect_whitespace_regex_pattern) == false && whitespace_detected == false) {
					outfile << input_line << std::endl;
					std::cout << "Output : " << input_line << std::endl;
					std::cout << "third case executed" << std::endl;
				}
				else if (std::regex_match(input_line, match, detect_whitespace_regex_pattern) && whitespace_detected ) {
					outfile << input_line << std::endl;
					std::cout << "Output : " << input_line << std::endl;
					std::cout << "fourth case executed" << std::endl;
				}
				
			}
			else std::cout << "Unable to open destination file";
		}
    infile.close();
	outfile.close();
	}
	else std::cout << "Unable to open source file";
  return 0;
};